import sys
from PyQt4 import Qt, QtCore, QtGui, QtHelp
import QHelpy

HELP_FILENAME = 'help_project.qhc'

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    splitter = QtGui.QSplitter()

    helpEngine = QtHelp.QHelpEngine(HELP_FILENAME)
    ok = helpEngine.setupData()
    print helpEngine.error()
    print ok

    contentModel = helpEngine.contentModel()
    contentWidget = helpEngine.contentWidget()
    indexModel = helpEngine.indexModel()
    indexWidget = helpEngine.indexWidget()
    helpViewer = QHelpy.HelpViewer(helpEngine)

    splitter.addWidget(contentWidget)
    splitter.addWidget(helpViewer)

    contentWidget.setModel(contentModel)
    indexWidget.setModel(indexModel)

    contentWidget.linkActivated.connect(helpViewer.setSource)

    splitter.show()

    app.exec_()
