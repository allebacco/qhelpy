'''
Alessandro Bacchini alessandro.bacchini@henesis.eu

Implementation of a QWebView that can manage Qt help files with QHelpEngine.
This implemenation is taken from QtCreator C++ sources and translated to
PyQt4

'''

from PyQt4 import Qt, QtCore, QtGui, QtHelp, QtWebKit, QtNetwork


def isLocalUrl(url):
    assert isinstance(url, QtCore.QUrl)

    scheme = str(url.scheme())
    if len(scheme) == 0 or \
            scheme == 'file' or \
            scheme == 'qrc' or \
            scheme == 'data' or \
            scheme == 'qthelp' or \
            scheme == 'about':
        return True
    return False


class HelpNetworkReply(QtNetwork.QNetworkReply):

    content = None
    offset = None

    def __init__(self, parent, request, fileData, mimeType):
        QtNetwork.QNetworkReply.__init__(self, parent=parent)
        assert isinstance(request, QtNetwork.QNetworkRequest)
        assert isinstance(fileData, str)
        self.content = fileData
        self.offset = 0
        self.setRequest(request)
        self.setOpenMode(QtCore.QIODevice.ReadOnly)
        self.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader, QtCore.QVariant(mimeType))
        self.setHeader(QtNetwork.QNetworkRequest.ContentLengthHeader, QtCore.QVariant(len(self.content)))
        QtCore.QTimer.singleShot(0, self.signalReadyRead)
        QtCore.QTimer.singleShot(0, self.signalFinished)

    def abort(self):
        pass

    @Qt.pyqtSlot()
    def signalReadyRead(self):
        self.readyRead.emit()

    @Qt.pyqtSlot()
    def signalFinished(self):
        self.finished.emit()

    def bytesAvailable(self):
        return len(self.content) - self.offset + QtNetwork.QNetworkReply.bytesAvailable(self)

    def readData(self, maxSize):
        if self.offset < len(self.content):
            end = min(self.offset + maxSize, len(self.content))
            data = self.content[self.offset:end]
            self.offset = end
            return data


class HelpNetworkAccessManager(QtNetwork.QNetworkAccessManager):

    helpEngine = None

    def __init__(self, helpEngine, parent=None):
        QtNetwork.QNetworkAccessManager.__init__(self, parent=parent)
        assert isinstance(helpEngine, QtHelp.QHelpEngine)

        self.helpEngine = helpEngine

    def createRequest(self, op, request, outgoingData=None):
        assert isinstance(request, QtNetwork.QNetworkRequest)

        scheme = str(request.url().scheme())
        if scheme == 'qthelp' or scheme == 'about':
            url = request.url()
            mimeType = str(url.toString())
            if mimeType.endswith('.svg') or \
                    mimeType.endswith('.svgz'):
                mimeType = 'image/svg+xml'
            elif mimeType.endswith('.css'):
                mimeType = 'text/css'
            elif mimeType.endswith('.js'):
                mimeType = 'text/javascript'
            else:
                mimeType = 'text/html'
            return HelpNetworkReply(self, request, str(self.helpEngine.fileData(url)), mimeType)
        return QtNetwork.QNetworkAccessManager.createRequest(self, op, request, outgoingData)


class HelpPage(QtWebKit.QWebPage):

    helpEngine = None

    def __init__(self, helpEngine, parent=None):
        QtWebKit.QWebPage.__init__(self, parent=parent)
        assert isinstance(helpEngine, QtHelp.QHelpEngine)

        self.helpEngine = helpEngine

    def createWindow(self, winTYpe):
        return None

    def acceptNavigationRequest(self, frame, request, navigationType):
        assert isinstance(request, QtNetwork.QNetworkRequest)

        url = request.url()
        if isLocalUrl(url):
            if str(url.path()).endswith('pdf'):
                filename = str(url.toString())
                tmpFilename = str(QtCore.QDir.tempPath())
                tmpFilename += str(QtCore.QDir.separator())
                tmpFilename += filename.split('/')[-1]
                filename = tmpFilename

                tmpFile = QtCore.QFile(QtCore.QDir.cleanPath(filename))
                if tmpFile.open(QtCore.QIODevice.ReadWrite):
                    tmpFile.write(self.helpEngine.fileData(url))
                    tmpFile.close()

                Qt.QDesktopServices.openUrl(QtCore.QUrl(tmpFile.fileName()))
                return False
            return True
        Qt.QDesktopServices.openUrl(url)
        return False


class HelpViewer(QtWebKit.QWebView):

    copyAvailable = Qt.pyqtSignal(bool)
    forwardAvailable = Qt.pyqtSignal(bool)
    backwardAvailable = Qt.pyqtSignal(bool)
    highlighted = Qt.pyqtSignal(str)
    sourceChanged = Qt.pyqtSignal(QtCore.QUrl)

    helpEngine = None
    homeUrl = None

    def __init__(self, helpEngine, parent=None):
        QtWebKit.QWebView.__init__(self, parent=parent)
        assert isinstance(helpEngine, QtHelp.QHelpEngine)

        self.helpEngine = helpEngine

        self.settings().setAttribute(QtWebKit.QWebSettings.PluginsEnabled, False)
        self.settings().setAttribute(QtWebKit.QWebSettings.JavaEnabled, False)
        self.setPage(HelpPage(helpEngine, self))

        self.page().setNetworkAccessManager(HelpNetworkAccessManager(helpEngine, parent=self))

        action = self.pageAction(QtWebKit.QWebPage.OpenLinkInNewWindow)
        action.setText("Open Link in New Tab")
        if not parent:
            action.setVisible(False)

        self.pageAction(QtWebKit.QWebPage.DownloadLinkToDisk).setVisible(False)
        self.pageAction(QtWebKit.QWebPage.DownloadImageToDisk).setVisible(False)
        self.pageAction(QtWebKit.QWebPage.OpenImageInNewWindow).setVisible(False)

        self.pageAction(QtWebKit.QWebPage.Copy).changed.connect(self.actionChanged)
        self.pageAction(QtWebKit.QWebPage.Back).changed.connect(self.actionChanged)
        self.pageAction(QtWebKit.QWebPage.Forward).changed.connect(self.actionChanged)

        self.page().linkHovered.connect(self.highlighted)
        self.urlChanged.connect(self.sourceChanged)

        self.setAcceptDrops(False)
        self.homeUrl = QtCore.QUrl()

    @Qt.pyqtSlot(QtCore.QUrl)
    def setSource(self, url):
        assert isinstance(url, QtCore.QUrl)
        if not self.homeUrl.isValid():
            self.homeUrl = url
        self.load(url)

    def source(self):
        return self.url()

    def documentTitle(self):
        return self.title()

    def hasSelection(self):
        return self.selectedText().isEmpty() == False

    def resetZoom(self):
        self.setTextSizeMultiplier(1.0)

    def zoomIn(self, zRange=1.):
        self.setTextSizeMultiplier(self.textSizeMultiplier() + zRange / 10.0)

    def zoomOut(self, zRange=1.):
        self.setTextSizeMultiplier(max(0.0, self.textSizeMultiplier() - zRange / 10.0))

    def copy(self):
        return self.triggerPageAction(QtWebKit.QWebPage.Copy)

    def isForwardAvailable(self):
        return self.pageAction(QtWebKit.QWebPage.Forward).isEnabled()

    def isBackwardAvailable(self):
        return self.pageAction(QtWebKit.QWebPage.Back).isEnabled()

    @Qt.pyqtSlot()
    def home(self):
        if self.homeUrl.isValid():
            self.setSource(self.homeUrl)

    @Qt.pyqtSlot()
    def backward(self):
        self.back()

    def wheelEvent(self, event):
        assert isinstance(event, QtGui.QWheelEvent)

        if event.modifiers() & Qt.Qt.ControlModifier:
            delta = event.delta()
            if delta > 0:
                self.zoomIn(delta / 120)
            elif delta < 0:
                self.zoomOut(-delta / 120)
            event.accept()
            return
        QtWebKit.QWebView.wheelEvent(self, event)

    def mouseReleaseEvent(self, event):
        assert isinstance(event, QtGui.QMouseEvent)

        if event.button() == Qt.Qt.XButton1:
            self.triggerPageAction(QtWebKit.QWebPage.Back)
            return
        if event.button() == Qt.Qt.XButton2:
            self.triggerPageAction(QtWebKit.QWebPage.Forward)
            return
        QtWebKit.QWebView.mouseReleaseEvent(self, event)

    @Qt.pyqtSlot()
    def actionChanged(self):
        action = self.sender()
        if action == self.pageAction(QtWebKit.QWebPage.Copy):
            self.copyAvailable.emit(action.isEnabled())
        elif action == self.pageAction(QtWebKit.QWebPage.Back):
            self.copyAvailable.emit(action.isEnabled())
        elif action == self.pageAction(QtWebKit.QWebPage.Forward):
            self.copyAvailable.emit(action.isEnabled())
